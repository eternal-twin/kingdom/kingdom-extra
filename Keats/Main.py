from Class_Unite import *
from Capitale import *
from Guerre import *
from Fonctions import *
from pprint import pprint


'''1er joueur'''
Ewen = Joueur('Ewen')
Balbui = Capitale('Balbui', 1,1, Ewen)
Thor = General('Thor', Ewen)



'''Simulation de la grille'''
for i in range(8):
    print(Balbui.grille[i])
    
print("Coups possibles")
print(coups_possibles(Balbui.grille))
#A toi de choisir un coup et de l'effectuer en faisant Balbui.tour(case1, case2, True), le True sert à voir chaque étape



'''Création de quelques lieux'''
# Vormir = Lieu('Vormir', 2,2, {Balbui : 3})
# Tello = Lieu('Tello', 5, 1)
# Minas = Lieu('Minas', 4, 3)
# Loba = Lieu('Loba', 3,3, {Tello:1, Minas:4, Vormir:2})
# Capo = Lieu('Capo', 7,6, {Loba : 1, Balbui : 4})



'''Simulation du commerce'''
# L = [Vormir, Tello, Minas, Loba, Capo]
# for l in L:
#     Ewen.territoires.append(l)
#     l.suzerain = Ewen
#     if l.is_connected(Balbui):
#         l.distance = len(dijsktra(graph, l, l.suzerain.capitale)) - 1
#     else : 
#         l.distance = 4
#     l.commerce = -l.distance**2  
#     for i in range(10):
#         l.armee.append(Soldat(Ewen, l))    
# Ewen.commerce = sum(t.commerce for t in Ewen.territoires)
# Balbui.gen_grille(True)
# for i in range(50):
#     a = Balbui.alignements_possibles()
#     Balbui.tour(a[-1][0], a[-1][1])
    


'''2e joueur'''
# Martin = Joueur('Martin')
# Entrefer = Capitale('Entrefer', 3,3, Martin)
# Loki = General('Loki', Martin)



'''Simulation de bataille'''
# for i in range(20):
#     Thor.armee.append(Paladin(Thor.joueur, Thor))
# for i in range(16):
#     Loki.armee.append(Soldat(Loki.joueur, Loki))
#     Loki.armee.append(Chevalier(Loki.joueur, Loki))  


Thor.attaquer(Loki)

