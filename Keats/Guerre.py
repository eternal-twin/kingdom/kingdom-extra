# import http.server
# import socketserver
# from urllib.parse import urlparse, parse_qs, unquote
# import json
# import sqlite3

from Class_Unite import *
from Capitale import *
from random import *
from pprint import pprint
from time import sleep


class General:
    def __init__(self, nom, joueur):
        self.joueur = joueur                #sous le controle de joueur
        self.joueur.generaux.append(self)
        self.loc = self.joueur.capitale     #se situe "loc"
        self.loc.generaux.append(self)
        
        self.nom = nom
        self.reputation = 1
        self.armee = []
        self.pos = None #'Attaque' ou 'Defense' lors d'une bataille
        self.fortifie = False #True or False 
    
        
    def partir(self, lieu):   #A compléter avec l'horodatage
        print(f"{self.nom} se déplace vers {lieu}")
        self.loc.generaux.remove(self)
        
        
    def avancer(self, lieu):   #A compléter avec l'horodatage, comment récupérer t la différence de temps ?
        if t < dt :
            t += 1
            self.loc = t/dt*self.loc.loc[0] + 1-t/dt*lieu.loc[0], t/dt*self.loc.loc[1] + 1-t/dt*lieu.loc[1] #Comb. linéaire
        else : #Si le temps nécéssaire a été dépassé, on considère le général comme arrivé à destination
            self.loc = lieu
            lieu.generaux.append(self)
        
        
    def attaquer(self, obj):
        print(f'{self.nom} attaque {obj.nom}')
        self.pos = 'Attaque'   #Pour repérer les camps
        obj.pos ='Defense'
        list_obj = [self, obj] #Liste des objets (général ou lieu) prenant part à la bataille. Comment faire varier cette liste au cours même d'une bataille ?
        battle = False  ##Est ce qu'il faut se battre ou le territoire n'est pas défendu ? 
        if type(obj).__name__ =='Lieu':
            for general in obj.generaux:
                if general.joueur != self.joueur and general.fortifie==True:
                    general.pos = 'Defense'
                    list_obj.append(general) #Si on général fortifie, il rejoint la bataille si le lieu est attaqué
                    battle = True 
        if obj.armee :
            obj.bataille = True #Statut du territoire : en guerre ou pas
            battle  = True
        if battle :
            bataille(list_obj) #On lance la bataille
        elif type(obj).__name__ == 'Lieu':     #Sinon on ajoute le territoire directement à ceux du joueur
            print(f"{self.nom} a conquis {obj.nom}")    
            obj.suzerain = self.joueur
            obj.suzerain.territoires.append(obj)
            obj.suzerain.gloire = len(obj.suzerain.territoires)
            if obj.suzerain.gloire > obj.suzerain.apogee :
                obj.suzerain.apogee = obj.suzerain.gloire
            obj.distance = len(dijsktra(graph, obj, obj.suzerain.capitale)) - 1
            obj.commerce = -(obj.distance)**2 #On initialise le commerce
        print(battle)
            
    
def bataille(list_obj):
    Attaque, Defense= [], [] #Liste des Généraux/Lieux en attaque/défense
    
    for obj in list_obj :
        if type(obj).__name__ == 'Capitale':
            mur = obj.batiments['Mur']   
        else:
            mur = 0
        if obj.pos == 'Attaque': #On sépare les camps en 2
            Attaque.append(obj)
        else :
            Defense.append(obj)
    
    A, D = sum(len(obj.armee) for obj in Attaque), sum(len(obj.armee) for obj in Defense) #Toutes les unités attaquantes, idem en défense
    A0, D0 = A, D  #Unités au début de la bataille
    M = max(len(obj.armee) for obj in list_obj) #Armée la plus grande
    m = min(len(obj.armee) for obj in list_obj) #Armée la plus petite
    n = max(1 , min([7, m, M//3+1 ]) -  mur) #Nombre de duels simultané
    Al, Dl = [], [] #Unités en duel
    
    for duel in range(n):
        a, d = randint(0,len(Attaque)-1), randint(0, len(Defense)-1) #On choisi un général/lieu au hasard
        au = randint(0, len(Attaque[a].armee)-1)
        du = randint(0, len(Defense[d].armee)-1) #On pioche une unité dans l'armée de ce général/lieu
        Al.append(Attaque[a].armee.pop(au)) #On l'enlève temporairament de l'armée et on l'envoie dans "l'armée au front"
        Dl.append(Defense[d].armee.pop(du))
       
    while A > 0 and D > 0:
        for duel in range(n):       
            Al[duel].attaquer(Dl[duel])   #Les unités s'attaquent mutuellement
            Dl[duel].attaquer(Al[duel])
            
            if Al[duel].HP <= 0 or Dl[duel].HP <= 0: 
                if Al[duel].HP > 0:
                    #print(f"Mort d'un {type(Dl[duel]).__name__} de {Dl[duel].loc.nom}")       
                    Dl[duel].loc = None    #Si une unité meurt on l'enlève la "désattache" de son général/lieu
                    Al[duel].loc.armee.append(Al[duel])    #On renvoie l'autre dans son armée
                    
                else :
                    #print(f"Mort d'un {type(Al[duel]).__name__} de {Al[duel].loc.nom}")
                    Al[duel].loc = None
                    Dl[duel].loc.armee.append(Dl[duel])      
                       
                    
                Dl[duel] = None  #Dans tous les cas on enlève les 2 adversaires du front 
                Al[duel] = None  #On fait pas del car on veut pas chenger la taille de la liste

        if None in Al+Dl:  #Tant que tous les duels ne sont pas occupés
            for L in [Al, Dl]:
                while None in L:
                    L.remove(None)
            
            #On met à jour les nombres d'unités et de duels
            A = sum(len(obj.armee) for obj in Attaque) + len(Al)
            D = sum(len(obj.armee) for obj in Defense) + len(Dl)
            M = max(len(obj.armee) + sum(int(u.loc==obj) for u in Al+Dl) for obj in list_obj) #Armée la plus grande
            m = min(len(obj.armee) + sum(int(u.loc==obj) for u in Al+Dl) for obj in list_obj) #Armée la plus petite
            n = max(1 , min([7, m, M//3+1 ]) -  mur) #Nombre de duels simultanés
    
            if A >= n and  len(Al) < n:  #Si il reste des soldats et qu'il y a un front non occupé
                for i in range(n-len(Al)):
                    a =  randint(0,len(Attaque)-1)   #On prend un général/lieu au hasard
                    au = randint(0, len(Attaque[a].armee)-1)  #On prend une unité au hasard parmi son armée
                    Al.append(Attaque[a].armee.pop(au))
                            
            if D >= n and len(Dl) < n:
                 for i in range(n-len(Dl)): 
                    d = randint(0, len(Defense)-1)
                    du = randint(0, len(Defense[d].armee)-1) #On renvoi des soldats au front   
                    Dl.append(Defense[d].armee.pop(du))
    
    if D == 0:
        Perdants = Defense
        Gagnants = Attaque
        print('Les assaillants ont gagné')
    else:
        Perdants = Attaque
        Gagnants = Defense
        print('Les défenseurs ont gagné')
        
    for obj in Gagnants: #AUGMENTATION DE LA REPUTATION
        if type(obj).__name__ == 'General' and obj.armee:
            rep = min(2, D0/(10*len(Attaque)))  #Formule arbitraire, rien dans la doc/IPK ?
            obj.reputation += rep
            print(f"Réputation de {obj.nom}: {obj.reputation}(+{rep})")
    
    for obj in list_obj : #MORT DES GENERAUX
        if type(obj).__name__ == 'General' and not obj.armee:
            obj.joueur.generaux.remove(obj)
            obj.loc.generaux.remove(obj)
            del obj
        
    for obj in Perdants :
        if type(obj).__name__ == 'Capitale' and obj in Perdants: #VASSALISATION
            obj.suzerain = Gagnants[0].joueur  #Gagnants[0] serait logiquement le 1er à lancer l'assault et qui est toujous vivant
            obj.joueur.reset()
            Gagnants[0].joueur.vassaux.append(obj.joueur)
                
        elif type(obj).__name__ == 'Lieu': #CONQUETE D'UN TERRITOIRE
            obj.suzerain = Gagnants[0].joueur
            obj.suzerain.territoires.append(obj)
            obj.suzerain.gloire = len(obj.suzerain.territoires)
            if obj.suzerain.gloire > obj.suzerain.apogee :
                obj.suzerain.apogee = obj.suzerain.gloire
            obj.distance = len(dijsktra(graph, obj, obj.suzerain.capitale)) - 1
            obj.commerce = -(obj.distance)**2
                    
          

        
        
# Instanciation et lancement du serveur
#
# httpd = socketserver.TCPServer(("", PORT), RequestHandler)
# print ("serveur sur port : {}".format(PORT))
# httpd.serve_forever()