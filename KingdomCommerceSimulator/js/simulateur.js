/* ------------
   Chart config
   ------------ */
   const LOOP_MAX = 5000;
   const prefs = {
	   // type: 'bar',
	   type: 'line',
	   data: {
		   datasets: [{
			   fill: {
				   target: 'origin',
				   above: '#00990066',   // Area will be red above the origin
				   below: '#99000066'    // And blue below the origin
			   },
		   }]
	   },
	   options: {
		   spanGaps: true,
		   showLine: true,
		   borderWidth: 1,
		   animation: false,
		   elements: {
			   point: {
				   radius: 0 // default to disabled in all datasets
			   }
		   },
		   interaction: {
			   mode: 'nearest',
			   axis: 'x',
			   intersect: false,
			   includeInvisible: false,
		   },
		   plugins: {
			   filler: {
				   propagate: true
			   },
			   legend: {
				   display: false
			   },
			   tooltip: {
				   displayColors: false,
			   }
		   },
		   scales: {
			   x: {
				   min: 0,
				   max: 500
			   },
			   y: {
				   min: 0,
				   type: 'linear',
			   }
		   }
	   }
   };
   
   /* ----------
	  Simulateur
	  ---------- */
   function Terre(commerce,soldat)
   {
	   this.commerce = commerce;
	   this.soldat = soldat;
	   this.compteur = commerce*(Math.abs(commerce)+1)*5;
	   this.gain = 0;
   }
   
   Terre.prototype.point = function(_turn)
   {
	   return {
		   turn: _turn,
		   commerce:this.commerce,
		   soldat:this.soldat,
		   gain:this.gain,
		   compteur:this.compteur
	   };
   }
   
   Terre.prototype.next = function()
   {
	   //mouais... dans ce cas on connait pas la formule
	   if(this.commerce<=0 && this.soldat===0)
	   {
		   this.commerce = this.compteur = undefined;
		   return;
	   }
	   
	   var diffsoldat = this.commerce>0 ? Math.floor(this.soldat-this.commerce/2) : this.soldat;
	   
	   if(diffsoldat<0)
	   {
		   //pas assez de soldat, la perte est le carré de la différence
		   this.compteur -= diffsoldat*diffsoldat;
	   }
	   else if(diffsoldat>0)
	   {
		   //le compteur augmente tranquillou
		   this.compteur += diffsoldat;
	   }
	   else
	   {
		   //ben le delta compteur est à 0 on n'arrete pas pour avoir tjrs le meme nombre d'entrée, mais on pourrait ...
		   //break;
	   }
	   
	   //on recalcule le commerce à partir du compteur grace à la formule magique
	   if(this.compteur>0)
	   {
		   this.commerce = Math.floor(Math.sqrt((this.compteur/5+0.25)) -0.5);
	   }
	   else
	   {
		   //doute, ce serait pas un -0.25 vu que le reste est inversé
		   this.commerce = Math.floor(-Math.sqrt((-this.compteur/5+0.25)) +0.5);
	   }
	   
	   this.gain += (this.commerce - this.soldat);
   }
   
   /* -----
	  Utils
	  ----- */
   function accord(nombre, pluriel, singulier)
   {
	   singulier = typeof(singulier) !== 'undefined' ? singulier : "";
	   return nombre > 1 ? pluriel : singulier;
   }
   
   function clear(id)
   {
	   let o = document.getElementById(id);
	   if(o && o.value) o.value = "";
   }
   
   function update(id, v)
   {
	   let o = document.getElementById(id);
	   if(o && o.value) o.value = v; 
   }
   
   function setCom_start()
   {
	   const inputDist = document.getElementById("distance");
	   inputDist.style.display = "none";
	   inputDist.value = null;
   }
   
   function setDistance()
   {
	   const inputDist = document.getElementById("distance");	
	   inputDist.style.display = "inline-block";
   }
   
   function buildShareLink(com, end, units)
   {
	   let tag = [];
	   
	   if(typeof(com) == "number" && !isNaN(com)) tag.push("com="+com);
	   
	   if(typeof(end) == "number" && !isNaN(end)) tag.push("end="+end);
   
	   if(typeof(units) == "number" && !isNaN(units)) tag.push("units="+units);
	   
	   if(tag.length > 0)
	   {
		   return "#" + tag.join("&");
	   }
	   return false;
   }
   
   /* -------
	  Process
	  ------- */
   let reach;
   let reachMax;
   function calc(commerce, soldat, commerce_end)
   {	
	   let terre = new Terre(commerce, soldat);
	   
	   let turn = 0;
	   let max = soldat *2 -1;
	   reach = undefined;
	   let datapoints = {
		   label: [],
		   data: []
	   }
	   
	   while(turn < LOOP_MAX)
	   {
		   let point = terre.point(turn);
		   datapoints.label.push(turn);
		   datapoints.data.push(point.commerce);
		   
		   //Lorsque l'objectif est atteint
		   if(!isNaN(commerce_end) && (
			   (commerce <= commerce_end && point.commerce >= commerce_end)
			|| (commerce >  commerce_end && point.commerce <= commerce_end)
		   ))
		   {
			   if(typeof(reach) == "undefined")
			   {
				   //Note du tour auquel l'objectif est atteint
				   reach = turn;
			   }
			   else if(reach *2 < turn
			   || (reach && turn > prefs.options.scales.x.max))
			   {
				   //Arrête la simuluation lorsque l'objectif est atteint
				   break;
			   }
		   }
		   //Lorque le commerce plafonne
		   else if (point.commerce == max)
		   {
			   if(typeof(reachMax) == "undefined")
			   {
				   //Note du tour auquel le commerce plafonne
				   reachMax = turn;
			   }
			   else if(reachMax *1.5 < turn
			   || (reachMax && turn > prefs.options.scales.x.max))
			   {
				   //Arrête la simuluation lorsque le commerce plafonne.
				   break;
			   }
		   }
		   
		   turn = turn +1;
		   terre.next();
	   }
		   
	   return datapoints;
   }
   
   function exe()
   {
	   const eCom_start = document.getElementById("com_start");
	   const eCom_end   = document.getElementById("com_end");
	   const eUnits     = document.getElementById("units");
	   const eGraph     = document.getElementById("graph");
	   const eUpUnits   = document.getElementById("up_units");
	   
	   const eMsg      = {
		   com_end: document.getElementById("warning_com_end"),
		   result:  document.getElementById("result"),
		   stable : document.getElementById("stable")
	   };
	   
	   let com_start = parseInt(eCom_start.value);
	   let com_end   = parseInt(eCom_end.value);
	   let units     = parseInt(eUnits.value);
	   let req       = Math.ceil(com_end * .5)
	   
	   let shareLink = buildShareLink(com_start, com_end, units);
	   
	   if(units < req)
	   {	
		   eUpUnits.innerHTML = "&uarr; " + req + " unité" + accord(req, "s");
		   eUpUnits.style.display = "inline-block";
		   eUpUnits.onclick = function()
		   {
			   eUnits.value = req;
			   this.innerHTML = "";
			   this.style.display = "none";
			   this.onclick = null;
			   exe();
		   };
	   }
	   else
	   {
		   eUpUnits.innerHTML = "";
		   eUpUnits.style.display = "none";
		   eUpUnits.onclick = null;
	   }
	   
	   //calc
	   let datas = calc(com_start, units, com_end);
		   
	   //infos
	   if(reach)
	   {
		   // if(last.y < com_end) need = ">"+need;
		   eMsg.result.innerHTML = reach + " tour" + accord(reach, "s") + " (avec " + units + " unité" + accord(units, "s") + ")";
	   }
	   else if(reachMax)
	   {
		   eMsg.result.innerHTML = reachMax + " tour" + accord(reachMax, "s") + " (avec " + units + " unité" + accord(units, "s") + ")";
	   }
	   else{
		   eMsg.result.innerHTML = "> " + LOOP_MAX;
	   }
	   
	   if(req)
	   {
		   eMsg.stable.innerHTML = req + " unité" + accord(req, "s") + " (pour maintenir le commerce à " + com_end + ")";
	   }
	   else eMsg.stable.innerHTML = "...";
		   
	   //Graphic
	   
	   //Reset canvas
	   var oldcanv = document.getElementById('myChart');
	   if(oldcanv) eGraph.removeChild(oldcanv)
	   var canv = document.createElement('canvas');
	   canv.id = 'myChart';
	   eGraph.appendChild(canv);
	   
	   let params = prefs;
	   params.data.labels = datas.label;
	   params.data.datasets[0].data = datas.data;
	   
	   if(!isNaN(com_end) && !isNaN(com_start))
	   {
		   params.options.scales.y.min = Math.min(com_start -1, com_end -1);
	   }
	   else if(!isNaN(com_start))
	   {
		   params.options.scales.y.min = com_start -1;
	   }
	   
	   params.options.plugins.tooltip.callbacks = {
		   title: function(context) {
			   return (context[0].parsed.y !== null) ? "Tour: " + context[0].parsed.x : "";
		   }
	   }
	   params.options.plugins.tooltip.callbacks.label = function(context) {
		   let str = 'Commerce: ';
		   if (context.parsed.y !== null) str += context.parsed.y;
		   return str;
	   };
	   
	   if(typeof(com_start) == "number" && ! isNaN(com_start))
	   {
		   params.options.plugins.tooltip.callbacks.footer = function(context) {
			   if (context[0].parsed.y !== null) str = context[0].parsed.y - com_start;
			   return "Recettes: " + str;
		   };
	   }
	   else
	   {
		   params.options.plugins.tooltip.callbacks.footer = function(context) {
			   return " ";
		   };
	   }	
   
	   new Chart("myChart", params);
	   
	   let eLink = document.getElementById('shareLink');
	   let sLink = document.getElementById('sLink');
	   if(shareLink)
	   {
		   sLink.value = "http://" + window.location.hostname + window.location.pathname + shareLink;
		   eLink.style.display = 'block';
	   }
	   else
	   {
		   sLink.value = null;
		   eLink.style.display = 'none';
	   }
   }
   
   /* +++++++++++++++++++++++++++++++++++++++++++++ */
   
   window.onload = function()
   {
	   if(window.location.hash.length > 0)
	   {
		   let u = false;
		   let re = /(com|end|units)=(-?\d+)/g;
		   let m;
		   
		   let com = null;
		   let end = null;
		   let unt = null;
		   
		   while ((m = re.exec(window.location.hash)) !== null)
		   {
			   if (m.index === re.lastIndex) re.lastIndex++;
			   
			   if      (m[1] == 'com')   com = parseInt(m[2]);
			   else if (m[1] == 'end')   end = parseInt(m[2]);
			   else if (m[1] == 'units') unt = parseInt(m[2]);
			   
		   }
		   
		   if(com || end || unt)
		   {
			   update('com_start', com);
			   update('com_end',   end);
			   update('units',     unt);
			   exe();
		   }
	   }
   };