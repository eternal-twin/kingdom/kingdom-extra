<?php
require("class".DIRECTORY_SEPARATOR."World.php");
require("class".DIRECTORY_SEPARATOR."Player.php");

//Params (default)
$log_file = "logs".DIRECTORY_SEPARATOR."Ranks_".date("Y-m-d");
$param_start_id = 21; // Maps 1~20 are invalid
$param_batch_size = 3676; // Highest map ID ever found ?
$param_output = false;
$param_empty  = false;
$param_invalid= false;

/**
 * Ranks page URL
 * @param int $mapID
 * @return string
 */
function url($n){
	return "http://kingdom.muxxu.com/map/{$n}/ranks";
}

//check params
if(isset($argc, $argv) && $argc > 1){
	for($i = 1; $i < $argc; $i++){
		//normalize arg
		$v = trim(strtolower($argv[$i]));
		
		// Help page
		if(in_array($v, array("help","h","-h","-help","--help"))){
			echo "\n".chr(205)." HELP: ".str_repeat(chr(205),42)."\n"
			.basename($argv[0], ".php")." run a crawler on ".url('{$n}')."\n\n"
			."Options:\n"
			."start <number>\n"
			."  Starts the crawler from the specified ID\n"
			."  Default: {$param_start_id}\n\n"
			."range <number>\n"
			."  Indicates the amount of worlds to explore\n"
			."  Default: {$param_batch_size}\n\n"
			."txt\n"
			."  Export results in a Text file\n\n"
			."csv\n"
			."  Export results in a CSV file (comma-separated)\n\n"
			."json\n"
			."  Export results in a JSON file\n\n"
			."empty\n"
			."  Include empty worlds\n\n"
			."invalid\n"
			."  Include invalid worlds\n\n"
			.str_repeat(chr(205),50)."\n";
			exit();
		}
		
		// Set Start ID
		elseif($v == "start"){
			if(!isset($argv[$i+1]) or !is_numeric($argv[$i+1])){
				echo "\nERROR: start option require numeric parameter\n\n"
				.">php ".$argv[0]." start 25\n"
				."  Crawl from world ID #25\n";
				exit();
			}
			$param_start_id = intval($argv[$i+1]);
			$i++;
		}
		
		// Set Range ID (Quatity)
		elseif($v == "range"){
			if(!isset($argv[$i+1]) or !is_numeric($argv[$i+1])){
				echo "\nERROR: range option require numeric parameter\n\n"
				.">php ".$argv[0]." range 100\n"
				."  Crawl up to 100 world IDs\n";
				exit();
			}
			$param_batch_size = intval($argv[$i+1]);
			$i++;
		}
		
		// File format
		elseif(in_array($v, array("txt","csv","json"))){
			if(!$param_output) $param_output = $v;
		}
		
		// Output empty worlds
		elseif($v == "empty"){
			$param_empty = true;
		}
		
		// Output invalid worlds
		elseif($v == "invalid"){
			$param_invalid = true;
		}
	}
}

/**
 * Get HTML source from Ranks page
 * @param int $n Map ID
 * @return string
 */
function get($n){
	return file_get_contents(url($n));
}

/**
 * Is a valid Map
 * @param string $source HTML source from Ranks page
 * @return bool
 */
function valid($source){
	$temp = array();
	preg_match('/<title>Kingdom\s+-\s+Classement\s*<\/title>/m', $source, $temp);
	return count($temp) > 0;
}

/**
 * Is a played Map (with players)
 * @param string $source HTML source from Ranks page
 * @return bool
 */
function isPlayed($source){
	$temp = array();
	preg_match('/<td colspan="6">Aucun joueur actuellement<\/td>/m', $source, $temp);
	return count($temp) == 0;
}


/**
 * Get Map name
 * @param string $source HTML source from Ranks page
 * @return bool
 */
function getMapName($source){
	preg_match('/<h1 class="imgstandard">Les Rois de ([^<]+)<\/h1>/m', $source, $temp);
	return (count($temp) > 0 && $temp[1]) ? $temp[1] : "?";
}

/**
 * Parse Ranks HTML sources
 * @param int $mapID
 * @param string $mapName
 * @param string $input_lines Ranks HTML source
 * @return World
 */
function parseRanks($mapID, $mapName, $input_lines){
	global $param_output;
	
	if($input_lines){
		preg_match_all('/<td><a href="\/user\/(?<uid>\d+)">(?<title>[^<]+)<\/a><\/td>[^<]*<td>[^<]+<strong>(?<glory>\d+)<\/strong> <img src="\/img\/icons\/l_up\.png"\/>[^<]+<\/td>[^<]*<td>[^<]+<strong>(?<reputation>\d+)<\/strong> <img src="\/img\/icons\/l_rep\.png"\/>[^<]+<\/td>[^<]*<td>[^<]+<strong[^>]*>(?<commerce>-?\d+)<\/strong> <img src="\/img\/icons\/small\/res_gold\.gif"\/>[^<]+<\/td>[^<]*<td>(?<age>[^<]+)<\/td>/m', $input_lines, $output_array, PREG_SET_ORDER);
	}
	
	$w = new World($mapID, $mapName, $param_output ? $param_output : "txt");
	
	if($input_lines){
		foreach($output_array as $p){
			$w->addPlayer( new Player($p) );
		}
	}
	
	return $w;
}

/**
 * Print buffer
 * @param array $buffer
 */
function output($buffer){
	echo "\n" . implode("\n",$buffer) . "\n";
}

// MAIN
echo "start\n";

// $bl = false; //carriage-return
$buffer = array(); //played maps
$buffer2 = array(); //empty maps
$buffer3 = array(); //invalid maps

define("TABLE_HEAD", "#ID__Name__________________Nb__Age_moy___med_Commerce__Glory___Rep__\n");
//	                 "---- --------------------|p:--|a:-----,-----|c:-------|g:-----|r:---"

// Crawl on each map IDs
echo TABLE_HEAD;
for($mapID = $param_start_id; $mapID <= $param_start_id + $param_batch_size; $mapID++){
	$data = get($mapID);
	$valid = valid($data);
	$played = $valid ? isPlayed($data) : false;
	
	// played Map
	if($valid && $played){
		$mapName = getMapName($data);
		$w = parseRanks($mapID, $mapName, $data);
		$w->valid = $valid;
		$buffer[] = $w;

		echo str_replace('|', chr(179), $w->output_txt())."\n";
	}
	// output empty maps if wanted
	elseif($param_empty && $valid && !$played){
		$mapName = getMapName($data);
		$w = parseRanks($mapID, $mapName, false);
		$w->valid = $valid;
		$buffer2[] = $w;
		
		echo str_replace('|', chr(179),sprintf("%- 4d %-20s|p: 0|             |         |       |     \n", $mapID, $mapName));
	}
	//putput invalid maps
	elseif($param_invalid && !$valid){
		$w = parseRanks($mapID, "<invalid>", false);
		$w->valid = $valid;
		$buffer3[] = $w;
		
		echo str_replace('|', chr(179),sprintf("%- 4d %-20s|    |             |         |       |     \n", $mapID, "<invalid>"));
	}
}

// Save in a file
if($param_output && $param_output == "txt"){
	$contents = "Ranks ".date("Y-m-d H:i:s")
	." | Crawer ids #".$param_start_id."~".($param_start_id + $param_batch_size)."\n"
	.TABLE_HEAD
	.implode("\n",$buffer);

	if($param_empty){
//		               "---- --------------------|p:--|a:-----,-----|c:-------|g:-----|r:---"
		$contents .= "\n_EMPTY_WORLDS______________Nb_______________________________________";
		if(count($buffer2)) $contents .= "\n".implode("\n",$buffer2);
	}
	if($param_invalid){
//		               "---- --------------------|p:--|a:-----,-----|c:-------|g:-----|r:---"
		$contents .= "\n_INVALID_WORLDS____________Nb_______________________________________";
		if(count($buffer3)) $contents .= "\n".implode("\n",$buffer3);
	}
	
	$log_file .= ".txt";
	$test = file_put_contents($log_file, $contents);
	echo $test ? "\nsaved as ".$log_file : "\nsave fail..";
}
elseif($param_output && $param_output == "csv"){
	$contents = "ID,Name (".$param_start_id."~".($param_start_id + $param_batch_size)."),Nb,Age moy,Age med,Commerce,Gloire,Reputation\n"
	.implode("\n",$buffer);

	if($param_empty && count($buffer2)) $contents .= "\n".implode("\n",$buffer2);
	if($param_invalid && count($buffer3)) $contents .= "\n".implode("\n",$buffer3);

	$log_file .= ".csv";
	$test = file_put_contents($log_file, $contents);
	echo $test ? "\nsaved as ".$log_file : "\nsave fail..";
}
elseif($param_output && $param_output == "json"){
	$contents = array(
		"date" => date("Y-m-d"),
		"time" => time(),
		"range" => array($param_start_id, $param_start_id + $param_batch_size),
		"data" => $buffer
	);
	
	if($param_empty && count($buffer2)) $contents["data"] = array_merge($contents["data"], $buffer2);
	if($param_invalid && count($buffer3)) $contents["data"] = array_merge($contents["data"], $buffer3);
	
	$log_file .= ".json";
	$test = file_put_contents($log_file.".json", json_encode($contents, JSON_PRETTY_PRINT));
	echo $test ? "\nsaved as ".$log_file : "\nsave fail..";
}

echo "\nTerminated\n";
