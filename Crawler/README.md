# Kingdom - Crawler ranks

Examine les tableaux de rangs de chaque mondes, en parcourant les `id` de chaque map.

## Description

Dresse la liste des mondes joués, compilant les détails suivants :

 * Nom du monde
 * Nombre de joueurs présents
 * Ages moyen, médian
 * Montant du commerce total
 * Total de gloire
 * Total de réputation

## Getting Started

### Pre-requis

PHP en ligne de commande: https://www.php.net/downloads

### Usage

`Crawler>php ranks_crawler.php`

`Crawler>php ranks_crawler.php json start 1`

Débuter l'exploration à partir de `id` n°1

`Crawler>php ranks_crawler.php json range 100`

Explorer 100 mondes.

`Crawler>php ranks_crawler.php json start 50 range 100`

Explorer 100 mondes à partir de `id` n°50.

`Crawler>php ranks_crawler.php txt`

Sauvegarde les résultats dans un fichier texte.

`Crawler>php ranks_crawler.php csv`

Sauvegarde les résultats dans un fichier csv (peut être importé dans un tableur).

`Crawler>php ranks_crawler.php json`

Sauvegarde les données complètes dans un fichier au format JSON.

`Crawler>php ranks_crawler.php empty`

Inclure les mondes vides.

`Crawler>php ranks_crawler.php invalid`

Inclure les mondes invalides.

`Crawler>php ranks_crawler.php json start 1 range 3999 empty invalid`

Sauvegarde dans un fichier au format JSON les données complètes des mondes 1 à 4000, incluant les mondes vides et invalides.

## Help

`Crawler>php ranks_crawler.php help`
```
HELP: 
ranks_crawler run a crawler on http://kingdom.muxxu.com/map/{$n}/ranks

Options:
start <number>
  Starts the crawler from the specified ID
  Default: 21

range <number>
  Indicates the amount of worlds to explore
  Default: 3676

txt
  Export results in a Text file

csv
  Export results in a CSV file (comma-separated)

json
  Export results in a JSON file

empty
  Include empty worlds

invalid
  Include invalid worlds
```

## Exemple de données

ID|Name|Nb|Age moy, med|Trade|Glory|Rep
---|---|---|---|---|---|---
21  |Lulert              |p: 2|a:  817,  640|c: 206760|g:  460|r: 93
22  |Parigues            |p: 1|a: 1440, 1440|c:  60188|g:   53|r:349
65  |Angileux            |p: 6|a:   56,   76|c:   4565|g:  133|r:145
189 |Ermailoi            |p: 8|a:   97,   85|c:  19878|g:  421|r:299
194 |Poissac             |p: 1|a:  247,  247|c:  42874|g:  343|r:152
200 |Rodeletz            |p: 3|a:   90,   36|c:  10429|g:  137|r: 64
204 |Auxenay             |p: 7|a:   67,   27|c:  24523|g:  221|r: 59
210 |Touleuille          |p: 2|a:   83,   27|c:   4139|g:  166|r: 31
225 |Yerailoi            |p:10|a:   42,   37|c:   3719|g:  345|r:121
233 |Poicolude           |p: 4|a:   90,   91|c:  18318|g:  285|r: 31
244 |Vaumirain           |p: 8|a:   44,   41|c:   1765|g:  210|r: 49
245 |Iseraye             |p: 5|a:   41,   35|c:    778|g:  123|r: 14
265 |Rachon              |p: 9|a:   56,   30|c:  37398|g:  419|r:184
278 |SpecialKE           |p: 1|a:   58,   58|c:    147|g:   23|r: 10
285 |Polaris             |p: 1|a:  161,  161|c:   9542|g:   97|r: 10
400 |Royaume des Beta    |p: 1|a: 1742, 1742|c:  21464|g:  192|r:  5
1328|Vae Victis II       |p: 1|a:   22,   22|c:      0|g:    1|r:  1
1394|Agomeux             |p: 9|a:   37,   39|c:   2932|g:  216|r: 25
1790|snake world         |p: 1|a:  303,  303|c:  80604|g:  319|r: 26
2382|Enkidiev            |p: 1|a:   23,   23|c:      0|g:    1|r:  1
3355|Tamriel             |p: 1|a:   81,   81|c:   4517|g:  102|r:  5
3534|Pseudo-publique 01  |p: 1|a:   75,   75|c:   1213|g:  100|r:  4
3671|Clem trop bg        |p: 3|a:   36,   36|c:    402|g:   58|r: 15
3694|Bubulle             |p: 1|a:   54,   54|c:    606|g:   40|r:  8
3696|Royaume de Ramski   |p: 1|a:   39,   39|c:    764|g:   39|r: 10
3697|Kikou               |p: 1|a:   38,   38|c:    -16|g:    8|r:  6

## Exemple de sauvegarde txt

```
Ranks 2023-02-05 17:58:55 | Crawer ids #21~3697
#ID__Name__________________Nb__Age_moy___med_Commerce__Glory___Rep__
21   Lulert              |p: 2|a:  817,  640|c: 206760|g:  460|r: 93
22   Parigues            |p: 1|a: 1440, 1440|c:  60188|g:   53|r:349
65   Angileux            |p: 6|a:   56,   76|c:   4565|g:  133|r:145
189  Ermailoi            |p: 8|a:   97,   85|c:  19878|g:  421|r:299
194  Poissac             |p: 1|a:  247,  247|c:  42874|g:  343|r:152
200  Rodeletz            |p: 3|a:   90,   36|c:  10429|g:  137|r: 64
204  Auxenay             |p: 7|a:   67,   27|c:  24523|g:  221|r: 59
210  Touleuille          |p: 2|a:   83,   27|c:   4139|g:  166|r: 31
225  Yerailoi            |p:10|a:   42,   37|c:   3719|g:  345|r:121
233  Poicolude           |p: 4|a:   90,   91|c:  18318|g:  285|r: 31
244  Vaumirain           |p: 8|a:   44,   41|c:   1765|g:  210|r: 49
245  Iseraye             |p: 5|a:   41,   35|c:    778|g:  123|r: 14
265  Rachon              |p: 9|a:   56,   30|c:  37398|g:  419|r:184
278  SpecialKE           |p: 1|a:   58,   58|c:    147|g:   23|r: 10
285  Polaris             |p: 1|a:  161,  161|c:   9542|g:   97|r: 10
400  Royaume des Beta    |p: 1|a: 1742, 1742|c:  21464|g:  192|r:  5
1328 Vae Victis II       |p: 1|a:   22,   22|c:      0|g:    1|r:  1
1394 Agomeux             |p: 9|a:   37,   39|c:   2932|g:  216|r: 25
1790 snake world         |p: 1|a:  303,  303|c:  80604|g:  319|r: 26
2382 Enkidiev            |p: 1|a:   23,   23|c:      0|g:    1|r:  1
3355 Tamriel             |p: 1|a:   81,   81|c:   4517|g:  102|r:  5
3534 Pseudo-publique 01  |p: 1|a:   75,   75|c:   1213|g:  100|r:  4
3671 Clem trop bg        |p: 3|a:   36,   36|c:    402|g:   58|r: 15
3694 Bubulle             |p: 1|a:   54,   54|c:    606|g:   40|r:  8
3696 Royaume de Ramski   |p: 1|a:   39,   39|c:    764|g:   39|r: 10
3697 Kikou               |p: 1|a:   38,   38|c:    -16|g:    8|r:  6
```

## Exemple de sauvegarde csv

```
ID,Name (21~3697),Nb,Age moy,Age med,Commerce,Gloire,Reputation
21,"Lulert",2,817,639,206999,460,93
22,"Parigues",1,1440,1440,60188,53,349
65,"Angileux",6,55,75,4565,133,145
189,"Ermailoi",8,97,84,19878,421,299
194,"Poissac",1,247,247,42874,343,152
200,"Rodeletz",3,89,36,10429,137,64
204,"Auxenay",7,66,26,24523,221,59
210,"Touleuille",2,82,26,4168,166,31
225,"Yerailoi",10,42,37,3719,345,121
233,"Poicolude",4,90,90,18318,285,31
244,"Vaumirain",8,43,41,1765,210,49
245,"Iseraye",5,41,35,778,123,14
265,"Rachon",9,56,30,37398,418,185
278,"SpecialKE",1,57,57,147,23,10
285,"Polaris",1,161,161,9542,97,10
400,"Royaume des Beta",1,1741,1741,21464,192,5
1328,"Vae Victis II",1,22,22,0,1,1
1394,"Agomeux",10,35,28,2932,217,25
1790,"snake world",1,303,303,80615,319,26
2382,"Enkidiev",1,23,23,0,1,1
3355,"Tamriel",1,80,80,4517,102,5
3534,"Pseudo-publique 01",1,75,75,1213,100,4
3671,"Clem trop bg",3,35,36,402,58,15
3694,"Bubulle",1,54,54,606,40,8
3696,"Royaume de Ramski",1,39,39,764,39,10
3697,"Kikou",1,37,37,-16,8,6
```

## Exemple de sauvegarde JSON

```json
{
    "date": "2023-02-05",
    "time": 1675620448,
    "range": [
        1,
        4000
    ],
    "data": [
        {
            "id": 21,
            "name": "Lulert",
            "players": [
                {
                    "uid": 5420000,
                    "title": "Empereur Aurel",
                    "glory": 459,
                    "reputation": 69,
                    "commerce": 206999,
                    "age": 994.8333333333334
                },
                {
                    "uid": 5420001,
                    "title": "Comte Hardy",
                    "glory": 1,
                    "reputation": 24,
                    "commerce": 0,
                    "age": 640.1666666666666
                }
            ],
            "valid": true,
            "pib": 206999,
            "glory": 460,
            "age_moy": 817.5,
            "age_med": 640.1666666666666,
            "reputation": 93
        }
	]
}
```

## Authors

nzun/abzolument
[@nzun](https://twinoid.com/user/111586)

## Version History

* 0.1
    * Initial Release

