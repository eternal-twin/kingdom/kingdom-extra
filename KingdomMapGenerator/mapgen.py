#!/usr/bin/env python3

"""
Kingdom map generator.

License:
    GNU GPLv3

    Copyright 2021 Aude Jeandroz 'Talsi-Eldermê'
    This program is free software: you can redistribute it and/or modify it under
    the terms of the GNU General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option) any later
    version.
    This program is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
    FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
    You should have received a copy of the GNU General Public License along with
    this program. If not, see <http://www.gnu.org/licenses/>.
"""

import mapanalysis
from kingdommap import KingdomMap


map_size_dict = {
    "XS": [1 * 128, 2 * 64],             # ~ 10 capitals
    "S": [2 * 128, 2 * 64],              # ~ 25 capitals
    "M": [3 * 128, 3 * 64],              # ~ 60 capitals
    "L": [3 * 128, 4 * 64],              # ~ 85 capitals
    "XL": [4 * 128, 5 * 64],             # ~ 150 capitals
    "XXL": [6 * 128, 6 * 64]             # ~ 280 capitals
}

def gen_map(language, map_size, id_map):
    width = map_size_dict[map_size][0]
    height = map_size_dict[map_size][1]
    kingdom_map = KingdomMap(language, map_size, width, height)
    kingdom_map.generate(
        margin = 16, \
        sample_coeff = 30, \
        min_dist_between_cities = 10, \
        max_dx = 30, \
        max_dy = 24,
        max_dist_between_path_and_city = 8, \
        max_long_edge_criteria = 30 \
    )
    kingdom_map.export_to_json(f"Maps/{id_map}.json")
    if not mapanalysis.check_links_consistency(kingdom_map.map_graph.cities):
        print("Error: inconsistent links in the map!")
    mapanalysis.print_statistics(kingdom_map.map_graph.cities)


def main():
    # generation of first set of maps. Edit this to generate new maps!!
    map_size = "M"
    first_id = 1
    nb_french_maps = 5
    nb_english_maps = 5
    nb_spanish_maps = 3
    nb_german_maps = 2

    for i in range(first_id, first_id + nb_french_maps):
        gen_map("french", map_size, i)
    first_id += nb_french_maps
    for i in range(first_id, first_id + nb_english_maps):
        gen_map("english", map_size, i)
    first_id += nb_english_maps
    for i in range(first_id, first_id + nb_spanish_maps):
        gen_map("spanish", map_size, i)
    first_id += nb_spanish_maps
    for i in range(first_id, first_id + nb_german_maps):
        gen_map("german", map_size, i)

if __name__ == "__main__":
    main()

